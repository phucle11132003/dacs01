﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTQLPK.GUI.Admin
{
    public partial class frm_admin : Form
    {
        public frm_admin()
        {
            InitializeComponent();
            UC_Hethong uc = new UC_Hethong();
            addUserControl(uc);

        }
        private void addUserControl(UserControl userControl)
        {
            userControl.Dock = DockStyle.Fill;
            Panelchucnang.Controls.Clear();
            Panelchucnang.Controls.Add(userControl);
            userControl.BringToFront();
        }
        private void btn_hethong_Click(object sender, EventArgs e)
        {
            UC_Hethong uc = new UC_Hethong();
            addUserControl(uc);
        }
        
        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_chuyenmon_Click(object sender, EventArgs e)
        {

        }

        private void btn_phongkham_Click(object sender, EventArgs e)
        {
            UC_Phongkham uc = new UC_Phongkham();
            addUserControl(uc);
        }

        private void panel_CNCT_Paint(object sender, PaintEventArgs e)
        {
            UC_CNnhanbenh uc = new UC_CNnhanbenh();

        }
    }
}

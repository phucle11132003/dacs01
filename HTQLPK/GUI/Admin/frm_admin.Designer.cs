﻿namespace HTQLPK.GUI.Admin
{
    partial class frm_admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_admin));
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox3 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.btn_trogiup = new Guna.UI2.WinForms.Guna2Button();
            this.btn_baocao = new Guna.UI2.WinForms.Guna2Button();
            this.btn_phongkham = new Guna.UI2.WinForms.Guna2Button();
            this.btn_chuyenmon = new Guna.UI2.WinForms.Guna2Button();
            this.btn_hethong = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.Panelchucnang = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.panel_CNCT = new System.Windows.Forms.Panel();
            this.guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.DockIndicatorTransparencyValue = 1D;
            this.guna2DragControl1.DragStartTransparencyValue = 1D;
            this.guna2DragControl1.TargetControl = this.guna2Panel1;
            this.guna2DragControl1.UseTransparentDrag = true;
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.guna2Panel1.BackColor = System.Drawing.Color.White;
            this.guna2Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.guna2Panel1.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Panel1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.guna2Panel1.Controls.Add(this.guna2Button1);
            this.guna2Panel1.Controls.Add(this.guna2ControlBox2);
            this.guna2Panel1.Controls.Add(this.guna2ControlBox3);
            this.guna2Panel1.Controls.Add(this.guna2ControlBox1);
            this.guna2Panel1.Controls.Add(this.btn_trogiup);
            this.guna2Panel1.Controls.Add(this.btn_baocao);
            this.guna2Panel1.Controls.Add(this.btn_phongkham);
            this.guna2Panel1.Controls.Add(this.btn_chuyenmon);
            this.guna2Panel1.Controls.Add(this.btn_hethong);
            this.guna2Panel1.Controls.Add(this.guna2PictureBox1);
            this.guna2Panel1.CustomBorderColor = System.Drawing.Color.Black;
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel1.FillColor = System.Drawing.Color.White;
            this.guna2Panel1.ForeColor = System.Drawing.Color.White;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(1920, 41);
            this.guna2Panel1.TabIndex = 3;
            // 
            // guna2Button1
            // 
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.White;
            this.guna2Button1.FocusedColor = System.Drawing.Color.White;
            this.guna2Button1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button1.ForeColor = System.Drawing.Color.Black;
            this.guna2Button1.Location = new System.Drawing.Point(362, 8);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.PressedColor = System.Drawing.Color.White;
            this.guna2Button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.guna2Button1.Size = new System.Drawing.Size(104, 24);
            this.guna2Button1.TabIndex = 4;
            this.guna2Button1.Text = "Nhà Thuốc";
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BackColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(1833, 0);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.Size = new System.Drawing.Size(44, 32);
            this.guna2ControlBox2.TabIndex = 1;
            // 
            // guna2ControlBox3
            // 
            this.guna2ControlBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox3.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2ControlBox3.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox3.Location = new System.Drawing.Point(1795, 0);
            this.guna2ControlBox3.Name = "guna2ControlBox3";
            this.guna2ControlBox3.PressedColor = System.Drawing.Color.White;
            this.guna2ControlBox3.Size = new System.Drawing.Size(44, 32);
            this.guna2ControlBox3.TabIndex = 2;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox1.Location = new System.Drawing.Point(1875, 0);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 32);
            this.guna2ControlBox1.TabIndex = 0;
            // 
            // btn_trogiup
            // 
            this.btn_trogiup.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_trogiup.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_trogiup.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_trogiup.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_trogiup.FillColor = System.Drawing.Color.White;
            this.btn_trogiup.FocusedColor = System.Drawing.Color.White;
            this.btn_trogiup.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_trogiup.ForeColor = System.Drawing.Color.Black;
            this.btn_trogiup.Location = new System.Drawing.Point(564, 11);
            this.btn_trogiup.Name = "btn_trogiup";
            this.btn_trogiup.PressedColor = System.Drawing.Color.White;
            this.btn_trogiup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_trogiup.Size = new System.Drawing.Size(88, 21);
            this.btn_trogiup.TabIndex = 6;
            this.btn_trogiup.Text = "Trợ Giúp";
            // 
            // btn_baocao
            // 
            this.btn_baocao.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_baocao.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_baocao.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_baocao.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_baocao.FillColor = System.Drawing.Color.White;
            this.btn_baocao.FocusedColor = System.Drawing.Color.White;
            this.btn_baocao.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_baocao.ForeColor = System.Drawing.Color.Black;
            this.btn_baocao.Location = new System.Drawing.Point(472, 8);
            this.btn_baocao.Name = "btn_baocao";
            this.btn_baocao.PressedColor = System.Drawing.Color.White;
            this.btn_baocao.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_baocao.Size = new System.Drawing.Size(86, 24);
            this.btn_baocao.TabIndex = 5;
            this.btn_baocao.Text = "Báo Cáo";
            // 
            // btn_phongkham
            // 
            this.btn_phongkham.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_phongkham.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_phongkham.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_phongkham.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_phongkham.FillColor = System.Drawing.Color.White;
            this.btn_phongkham.FocusedColor = System.Drawing.Color.White;
            this.btn_phongkham.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_phongkham.ForeColor = System.Drawing.Color.Black;
            this.btn_phongkham.Location = new System.Drawing.Point(244, 8);
            this.btn_phongkham.Name = "btn_phongkham";
            this.btn_phongkham.PressedColor = System.Drawing.Color.White;
            this.btn_phongkham.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_phongkham.Size = new System.Drawing.Size(112, 24);
            this.btn_phongkham.TabIndex = 3;
            this.btn_phongkham.Text = "Phòng Khám";
            this.btn_phongkham.Click += new System.EventHandler(this.btn_phongkham_Click);
            // 
            // btn_chuyenmon
            // 
            this.btn_chuyenmon.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_chuyenmon.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_chuyenmon.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_chuyenmon.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_chuyenmon.FillColor = System.Drawing.Color.White;
            this.btn_chuyenmon.FocusedColor = System.Drawing.Color.White;
            this.btn_chuyenmon.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_chuyenmon.ForeColor = System.Drawing.Color.Black;
            this.btn_chuyenmon.Location = new System.Drawing.Point(151, 8);
            this.btn_chuyenmon.Name = "btn_chuyenmon";
            this.btn_chuyenmon.PressedColor = System.Drawing.Color.White;
            this.btn_chuyenmon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_chuyenmon.Size = new System.Drawing.Size(88, 24);
            this.btn_chuyenmon.TabIndex = 2;
            this.btn_chuyenmon.Text = "Đối Tác";
            this.btn_chuyenmon.Click += new System.EventHandler(this.btn_chuyenmon_Click);
            // 
            // btn_hethong
            // 
            this.btn_hethong.BorderColor = System.Drawing.SystemColors.Desktop;
            this.btn_hethong.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_hethong.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_hethong.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_hethong.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_hethong.FillColor = System.Drawing.Color.White;
            this.btn_hethong.FocusedColor = System.Drawing.Color.White;
            this.btn_hethong.Font = new System.Drawing.Font("Arial", 10.2F);
            this.btn_hethong.ForeColor = System.Drawing.Color.Black;
            this.btn_hethong.Location = new System.Drawing.Point(46, 8);
            this.btn_hethong.Name = "btn_hethong";
            this.btn_hethong.PressedColor = System.Drawing.Color.White;
            this.btn_hethong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_hethong.Size = new System.Drawing.Size(99, 24);
            this.btn_hethong.TabIndex = 1;
            this.btn_hethong.Tag = "1";
            this.btn_hethong.Text = "Hệ Thống";
            this.btn_hethong.Click += new System.EventHandler(this.btn_hethong_Click);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.Image")));
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(3, 3);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(45, 28);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 6;
            this.guna2PictureBox1.TabStop = false;
            // 
            // Panelchucnang
            // 
            this.Panelchucnang.BackColor = System.Drawing.Color.White;
            this.Panelchucnang.BorderColor = System.Drawing.Color.Black;
            this.Panelchucnang.Location = new System.Drawing.Point(0, 38);
            this.Panelchucnang.Name = "Panelchucnang";
            this.Panelchucnang.Size = new System.Drawing.Size(1920, 124);
            this.Panelchucnang.TabIndex = 1;
            this.Panelchucnang.Paint += new System.Windows.Forms.PaintEventHandler(this.guna2Panel2_Paint);
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // panel_CNCT
            // 
            this.panel_CNCT.BackColor = System.Drawing.Color.Silver;
            this.panel_CNCT.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_CNCT.Location = new System.Drawing.Point(0, 158);
            this.panel_CNCT.Margin = new System.Windows.Forms.Padding(2);
            this.panel_CNCT.Name = "panel_CNCT";
            this.panel_CNCT.Size = new System.Drawing.Size(1920, 922);
            this.panel_CNCT.TabIndex = 4;
            this.panel_CNCT.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_CNCT_Paint);
            // 
            // frm_admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.guna2Panel1);
            this.Controls.Add(this.panel_CNCT);
            this.Controls.Add(this.Panelchucnang);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frm_admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_khung";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.guna2Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Button btn_trogiup;
        private Guna.UI2.WinForms.Guna2Button btn_baocao;
        private Guna.UI2.WinForms.Guna2Button btn_phongkham;
        private Guna.UI2.WinForms.Guna2Button btn_chuyenmon;
        private Guna.UI2.WinForms.Guna2Button btn_hethong;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2Panel Panelchucnang;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private System.Windows.Forms.Panel panel_CNCT;
    }
}